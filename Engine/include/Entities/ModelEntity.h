#ifndef _MODELENTITY_H_
#define _MODELENTITY_H_

#include "GenericEntity.h"

class ModelEntity : public GenericEntity
{
public:
	ModelEntity();
	~ModelEntity(void);

	bool Render(D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, D3DManager*);

	bool isLoaded();

	Model* m_Model;
	TextureShader* m_Shader;
	Texture* m_Texture;
};

#endif