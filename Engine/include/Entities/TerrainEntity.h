#ifndef _TERRAINENTITY_H_
#define _TERRAINENTITY_H_

#include "GenericEntity.h"

class TerrainEntity : public GenericEntity
{
public:
	TerrainEntity();
	~TerrainEntity(void);

	bool Render(D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, D3DManager*);

	bool isLoaded();

	Terrain* m_Terrain;
	ColorShader* m_Shader;
};

#endif