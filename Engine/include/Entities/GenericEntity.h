#ifndef _GENERICENTITY_H_
#define _GENERICENTITY_H_

#include "..\ResourceTypes\Model.h"
#include "..\ResourceTypes\TextureShader.h"
#include "..\ResourceTypes\ColorShader.h"
#include "..\ResourceTypes\Texture.h"
#include "..\ResourceTypes\Terrain.h"

#include "..\Rendering\D3DManager.h"

class GenericEntity
{
public:
	virtual bool Render(D3DXMATRIX, D3DXMATRIX, D3DXMATRIX, D3DManager*) = 0;

	virtual bool isLoaded() = 0;

	D3DXVECTOR3 m_Position;
protected:
	GenericEntity();
	virtual ~GenericEntity(void);
};

#endif