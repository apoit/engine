#ifndef _TERRAIN_H_
#define _TERRAIN_H_

#include <d3d11.h>
#include <d3dx10math.h>
#include <stdio.h>

#include "GenericResource.h"

class Terrain : public GenericResource
{
private:
	struct VertexType
	{
		D3DXVECTOR3 position;
		D3DXVECTOR3 normal;
	};

	struct HeightMapType 
	{ 
		float x, y, z;
		float nx, ny, nz;
	};

	struct VectorType 
	{ 
		float x, y, z;
	};

public:
	Terrain(wchar_t*, ID3D11Device*, HWND);
	~Terrain(void);

	bool Initialize();
	void Shutdown();
	void Render(ID3D11DeviceContext*);

	int GetIndexCount();

private:
	bool LoadHeightMap(std::wstring);
	void NormalizeHeightMap();
	bool CalculateNormals();
	void ShutdownHeightMap();

	bool InitializeBuffers(ID3D11Device*);
	void ShutdownBuffers();
	void RenderBuffers(ID3D11DeviceContext*);

	int m_terrainWidth, m_terrainHeight;
	int m_vertexCount, m_indexCount;
	ID3D11Buffer *m_vertexBuffer, *m_indexBuffer;

	HeightMapType* m_heightMap;

	ID3D11Device* m_device;
	HWND m_hwnd;
};

#endif