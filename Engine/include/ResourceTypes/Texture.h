#ifndef _TEXTURE_H_
#define _TEXTURE_H_

#include <d3d11.h>
#include <d3dx11tex.h>

#include "GenericResource.h"

class Texture : public GenericResource
{
public:
	Texture(wchar_t*, ID3D11Device*, HWND);
	~Texture(void);

	bool Initialize();
	void Shutdown();

	ID3D11ShaderResourceView* GetTexture();

private:
	ID3D11ShaderResourceView* m_texture;

	ID3D11Device* m_device;
	HWND m_hwnd;
};

#endif