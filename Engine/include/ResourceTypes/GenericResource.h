#ifndef _GENERICRESOURCE_H_
#define _GENERICRESOURCE_H_

#include <string>

class GenericResource
{
public:
	virtual bool Initialize() = 0;
	virtual ~GenericResource();
	bool isLoaded();

protected:
	GenericResource();
	GenericResource(wchar_t*);
	GenericResource(wchar_t*, wchar_t*);

	std::wstring m_filePath;
	std::wstring m_filePath2;

	bool m_loaded;
};

#endif