#ifndef _FONT_H_
#define _FONT_H_

#include <d3d11.h>
#include <d3dx10math.h>
#include <fstream>

#include "GenericResource.h"
#include "Texture.h"

class Font : public GenericResource
{
public:
	Font(wchar_t*, wchar_t*, ID3D11Device*, HWND);
	~Font();

	bool Initialize();
	void Shutdown();

	ID3D11ShaderResourceView* GetTexture();
	void BuildVertexArray(void*, char*, float, float);

private:
	struct FontType
	{
		float left, right;
		int size;
	};

	struct VertexType
	{
		D3DXVECTOR3 position;
		D3DXVECTOR2 texture;
	};

	bool LoadFontData(std::wstring);
	void ReleaseFontData();
	bool LoadTexture(std::wstring);
	void ReleaseTexture();

	FontType* m_Font;
	Texture* m_Texture;

	ID3D11Device* m_device;
	HWND m_hwnd;
};

#endif