#ifndef _RENDERMANAGER_H_
#define _RENDERMANAGER_H_

#include <Windows.h>

#include "D3DManager.h"
#include "..\Scene\Scene.h"
#include "..\Scene\Camera.h"
#include "..\ResourceTypes\Model.h"
#include "..\ResourceTypes\TextureShader.h"

// Globals
const bool FULL_SCREEN = false;
const bool VSYNC_ENABLED = false;
const float SCREEN_DEPTH = 1000.0f;
const float SCREEN_NEAR = 0.1f;

class RenderManager
{
public:
	RenderManager(void);
	~RenderManager(void);

	bool Initialize(int, int, HWND);
	void Shutdown();
	bool Draw(Scene*);

	D3DManager* getD3D();

private:
	bool Render(Scene*);

	D3DManager* m_D3D;
};

#endif