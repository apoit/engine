#ifndef _GAMELOGIC_H_
#define _GAMELOGIC_H_

#include "..\ResourceManagement\ResourceManager.h"
#include "..\Rendering\RenderManager.h"
#include "..\System\InputManager.h"
#include "..\System\Fps.h"

#include "..\Scene\Scene.h"
#include "..\Scene\Camera.h"
#include "..\Scene\Text.h"
#include "..\ResourceTypes\Model.h"
#include "..\ResourceTypes\TextureShader.h"
#include "..\ResourceTypes\Font.h"
#include "..\ResourceTypes\FontShader.h"

#include <sstream>

class GameLogic
{
public:
	GameLogic(ResourceManager*, RenderManager*, Fps*);
	~GameLogic(void);

	void Initialize();
	void Update(InputManager*, float);
	void Shutdown();

	Scene* getScene();

private:
	Scene* m_Scene;
	ResourceManager* m_ResourceManager;
	RenderManager* m_RenderManager;
	Fps* m_Fps;
};

#endif