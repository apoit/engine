#ifndef _SYSTEMMANAGER_H_
#define _SYSTEMMANAGER_H_

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

#include <Windows.h>
#include <windowsx.h>

#include "..\Rendering\RenderManager.h"
#include "..\ResourceManagement\ResourceManager.h"
#include "ThreadManager.h"
#include "InputManager.h"
#include "Fps.h"
#include "Timer.h"

#include "..\Game\GameLogic.h"

class SystemManager
{
public:
	SystemManager(int);
	~SystemManager(void);

	bool Initialize();
	void Run();
	void Shutdown();

	void InitializeWindow(int&, int&);
	void ShutdownWindows();

private:
	HINSTANCE m_hInstance;
	HWND m_hwnd;
	LPCWSTR m_applicationName;
	int m_nShowCmd;

	ThreadManager* m_ThreadManager;
	RenderManager* m_RenderManager;
	ResourceManager* m_ResourceManager;
	InputManager* m_InputManager;
	Fps* m_Fps;
	Timer* m_Timer;
};

// the WindowProc function prototype
LRESULT CALLBACK WindowProc(HWND hWnd,
                         UINT message,
                         WPARAM wParam,
                         LPARAM lParam);

#endif