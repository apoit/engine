/* Thread pool based on article found at http://progsch.net/wordpress/?p=81 */

#ifndef _THREADMANAGER_H_
#define _THREADMANAGER_H_

#include <thread>
#include <mutex>
#include <condition_variable>
#include <vector>
#include <deque>

class ThreadManager;

// our worker thread objects
class Worker {
public:
    Worker(ThreadManager &s) : pool(s) { }
    void operator()();
private:
    ThreadManager &pool;
};

class ThreadManager
{
public:
	ThreadManager();
	~ThreadManager();

	bool Initialize(size_t);
	void Shutdown();

    void enqueue(std::function<void()>);  
	bool isStopped();

private:
    friend class Worker;
 
    // need to keep track of threads so we can join them
    std::vector< std::thread > workers;
 
    // the task queue
    std::deque< std::function<void()> > tasks;
 
    // synchronization
    std::mutex queue_mutex;
    std::condition_variable condition;
    bool stop;
};

#endif

