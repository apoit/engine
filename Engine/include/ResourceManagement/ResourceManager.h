#ifndef _RESOURCEMANAGER_H_
#define _RESOURCEMANAGER_H_

#include <vector>
#include <thread>

#include "..\ResourceTypes\GenericResource.h"
#include "..\ResourceTypes\TextureShader.h"
#include "..\ResourceTypes\Model.h"
#include "..\ResourceTypes\Texture.h"
#include "..\ResourceTypes\Font.h"

#include "..\System\ThreadManager.h"
#include "..\Rendering\RenderManager.h"

class ResourceManager
{
public:
	ResourceManager(void);
	~ResourceManager(void);

	bool Initialize(RenderManager*, ThreadManager*, HWND);
	void Shutdown();
	bool Draw();

	// Function to load a resource with no file requirement
	template <class T>
	T* newResource () {
		T* temp = new T(m_RenderManager->getD3D()->GetDevice(), m_hwnd);

		std::function<void()> f = [=] {
			temp->Initialize();
		};

		m_ThreadManager->enqueue(f);

		resources.push_back(temp);
		return temp;
	}

	// Function to load a resource with one file requirement
	template <class T>
	T* newResource (wchar_t* file) {
		T* temp = new T(file, m_RenderManager->getD3D()->GetDevice(), m_hwnd);

		std::function<void()> f = [=] {
			temp->Initialize();
		};

		m_ThreadManager->enqueue(f);

		resources.push_back(temp);
		return temp;
	}

	// Function to load a resource with two file requirement
	template <class T>
	T* newResource (wchar_t* file, wchar_t* file2) {
		T* temp = new T(file, file2, m_RenderManager->getD3D()->GetDevice(), m_hwnd);

		std::function<void()> f = [=] {
			temp->Initialize();
		};

		m_ThreadManager->enqueue(f);

		resources.push_back(temp);
		return temp;
	}

private:
	std::vector<GenericResource*> resources;

	RenderManager* m_RenderManager;
	ThreadManager* m_ThreadManager;
	HWND m_hwnd;
};

#endif