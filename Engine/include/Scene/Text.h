#ifndef _TEXT_H_
#define _TEXT_H_

#include "..\ResourceTypes\FontShader.h"
#include "..\ResourceTypes\Font.h"

#include "..\Rendering\D3DManager.h"
#include "Camera.h"

class Text
{
private:
	struct SentenceType
	{
		ID3D11Buffer *vertexBuffer, *indexBuffer;
		int vertexCount, indexCount, maxLength;
		float red, green, blue;
	};

	struct VertexType
	{
		D3DXVECTOR3 position;
		D3DXVECTOR2 texture;
	};

public:
	Text(void);
	~Text(void);

	bool InitializeSentence(int, D3DManager*);
	void UpdateSentence(std::string, int, int, float, float, float);

	bool Render(D3DXMATRIX, D3DXMATRIX);

	bool isLoaded();

	Font* m_Font;
	FontShader* m_Shader;

private:
	bool UpdateBuffers();
	bool RenderSentence(D3DXMATRIX, D3DXMATRIX);

	SentenceType* m_Sentence;
	std::string m_Text;
	int m_PositionX, m_PositionY;
	float m_red, m_green, m_blue;

	bool isUpdated;

	D3DManager* m_D3D;

	D3DXMATRIX m_baseViewMatrix;
};

#endif