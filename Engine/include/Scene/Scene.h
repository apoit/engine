#ifndef _SCENE_H_
#define _SCENE_H_

#include "../Entities/GenericEntity.h"
#include "../Entities/ModelEntity.h"
#include "../Entities//TerrainEntity.h"
#include "Camera.h"
#include "Text.h"
#include <vector>

class Scene
{
public:
	Scene(void);
	~Scene(void);

	bool Initialize();
	void Shutdown();
	bool Draw();

	bool isLoaded();

	Camera* m_Camera;
	std::vector<GenericEntity*> m_Entities;
	std::vector<Text*> m_Texts;
};

#endif

