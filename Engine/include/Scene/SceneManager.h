#ifndef _SCENEMANAGER_H_
#define _SCENEMANAGER_H_

#include "..\Rendering\RenderManager.h"
#include "Scene.h"

class SceneManager
{
public:
	SceneManager(void);
	~SceneManager(void);

	bool Initialize(RenderManager*, HWND);
	void Shutdown();
	bool Draw();

private:
	RenderManager* m_RenderManager;
	HWND m_hwnd;

	Scene* m_Scene;
};

#endif