#ifndef _CAMERA_H_
#define _CAMERA_H_

#include <d3dx10math.h>

#include "..\System\InputManager.h"

class Camera
{
public:
	Camera(void);
	~Camera(void);

	void SetPosition(float, float, float);
	void SetRotation(float, float, float);

	D3DXVECTOR3 GetPosition();
	D3DXVECTOR3 GetRotation();

	void Render();
	void Update(InputManager*, float);
	void GetViewMatrix(D3DXMATRIX&);

	void MoveForward(float);
	void MoveBackward(float);
	void MoveLeft(float);
	void MoveRight(float);
	void MoveUp(float);
	void MoveDown(float);

private:
	D3DXVECTOR3 m_position;
	D3DXVECTOR3 m_rotation;
	D3DXVECTOR3 m_lookat;
	D3DXVECTOR3 m_up;

	D3DXVECTOR3 m_lookAtRot;
	D3DXVECTOR3 m_upRot;

	D3DXMATRIX m_viewMatrix;
};

#endif