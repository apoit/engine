Texture2D shaderTexture;
SamplerState SampleType;

struct PixelInputType
{
    float4 position : SV_POSITION;
	float3 normal : NORMAL;
	float2 tex : TEXCOORD0;
};

float4 ModelPixelShader(PixelInputType input) : SV_TARGET
{
	float lightIntensity;
	float4 color, textureColor;

	 // Invert the light direction for calculations.
    float3 lightDir = normalize(-float3(0.0f, -0.5f, 1.0f));

	// Calculate the amount of light on this pixel.
    lightIntensity = saturate(dot(input.normal, lightDir));

	 // Sample the pixel color from the texture using the sampler at this texture coordinate location.
    textureColor = shaderTexture.Sample(SampleType, input.tex);

	if(input.tex.x == -1.0f) {
		color = saturate(float4(1.0f, 0.0f, 0.0f, 1.0f) * (lightIntensity + 0.2));
	}
	else {
		color = saturate(textureColor * (lightIntensity + 0.2));
	}

    return color;
}