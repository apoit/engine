#include "include\System\SystemManager.h"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd)
{
	SystemManager* m_System;
	bool result;

	m_System = new SystemManager(nShowCmd);
	if(!m_System) return 0;

	result = m_System->Initialize();
	if(result) { 
		m_System->Run(); 
	}

	m_System->Shutdown();
	delete m_System;

	return 0;
}