#include "..\..\include\ResourceTypes\Texture.h"


Texture::Texture(wchar_t* filePath, ID3D11Device* device, HWND hwnd)
	: GenericResource(filePath)
{
	m_texture = 0;

	m_device = device;
	m_hwnd = hwnd;
}


Texture::~Texture(void)
{
}

bool Texture::Initialize() {
	HRESULT result;

	// Load the texture in.
	result = D3DX11CreateShaderResourceViewFromFile(m_device, m_filePath.c_str(), NULL, NULL, &m_texture, NULL);
	if(FAILED(result))
	{
		MessageBox(m_hwnd, m_filePath.c_str(), L"Missing Texture File", MB_OK);
		throw "Could not load your texture";
		return false;
	}

	m_loaded = true;

	return true;
}

void Texture::Shutdown() {
	// Release the texture resource.
	if(m_texture)
	{
		m_texture->Release();
		m_texture = 0;
	}

	return;
}

ID3D11ShaderResourceView* Texture::GetTexture()
{
	return m_texture;
}