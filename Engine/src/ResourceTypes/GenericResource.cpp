#include "..\..\include\ResourceTypes\GenericResource.h"

GenericResource::GenericResource()
{
	m_loaded = false;
}

GenericResource::GenericResource(wchar_t* filePath)
{
	m_filePath = filePath;
	m_loaded = false;
}

GenericResource::GenericResource(wchar_t* filePath, wchar_t* filePath2)
{
	m_filePath = filePath;
	m_filePath2 = filePath2;
	m_loaded = false;
}


GenericResource::~GenericResource(void)
{
	
}

bool GenericResource::isLoaded() {
	return m_loaded;
}