#include "..\..\include\Scene\Camera.h"


Camera::Camera(void)
{
	m_position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_rotation = D3DXVECTOR3(0.0f, 0.0f, 0.0f);
	m_lookat   = D3DXVECTOR3(0.0f, 0.0f, 1.0f);
	m_up       = D3DXVECTOR3(0.0f, 1.0f, 0.0f);

	m_lookAtRot = D3DXVECTOR3(0.0f, 0.0f, 1.0f);
	m_upRot     = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
}


Camera::~Camera(void)
{
}

void Camera::SetPosition(float x, float y, float z)
{
	m_position.x = x;
	m_position.y = y;
	m_position.z = z;
	return;
}

void Camera::SetRotation(float x, float y, float z)
{
	m_rotation.x = x;
	m_rotation.y = y;
	m_rotation.z = z;
	return;
}

D3DXVECTOR3 Camera::GetPosition()
{
	return m_position;
}


D3DXVECTOR3 Camera::GetRotation()
{
	return m_rotation;
}

void Camera::MoveForward(float distance)
{
	m_position += m_lookAtRot * distance;
}

void Camera::MoveBackward(float distance)
{
	m_position -= m_lookAtRot * distance;
}

void Camera::MoveLeft(float distance) {
	D3DXVECTOR3 left;

	// Cross our lookat and up vectors to obtain our left vector
	D3DXVec3Cross(&left, &m_lookAtRot, &m_upRot);

	m_position += left * distance;
}

void Camera::MoveRight(float distance) {
	D3DXVECTOR3 right;

	// Cross our lookat and up vectors to obtain our left vector
	D3DXVec3Cross(&right, &m_upRot, &m_lookAtRot);

	m_position += right * distance;
}

void Camera::MoveUp(float distance) {
	m_position += m_upRot * distance;
}

void Camera::MoveDown(float distance) {
	m_position -= m_upRot * distance;
}

void Camera::Render()
{
	D3DXVECTOR3 lookAtFinal;
	float yaw, pitch, roll;
	D3DXMATRIX rotationMatrix;

	// Set the yaw (Y axis), pitch (X axis), and roll (Z axis) rotations in radians.
	pitch = m_rotation.x * 0.0174532925f;
	yaw   = m_rotation.y * 0.0174532925f;
	roll  = m_rotation.z * 0.0174532925f;

	// Create the rotation matrix from the yaw, pitch, and roll values.
	D3DXMatrixRotationYawPitchRoll(&rotationMatrix, yaw, pitch, roll);

	// Transform the lookAt and up vector by the rotation matrix so the view is correctly rotated at the origin.
	D3DXVec3TransformCoord(&m_lookAtRot, &m_lookat, &rotationMatrix);
	D3DXVec3TransformCoord(&m_upRot, &m_up, &rotationMatrix);

	// Translate the rotated camera position to the location of the viewer.
	lookAtFinal = m_position + m_lookAtRot;

	// Finally create the view matrix from the three updated vectors.
	D3DXMatrixLookAtLH(&m_viewMatrix, &m_position, &lookAtFinal, &m_upRot);

	return;
}

void Camera::Update(InputManager* input, float frameTime) {
	// Handle keyboard commands

	float distance = frameTime * 0.01f;

	if(input->IsKeyPressed(DIK_W)) {
		MoveForward(distance);
	}
	else if(input->IsKeyPressed(DIK_S)) {
		MoveBackward(distance);
	}

	if(input->IsKeyPressed(DIK_A)) {
		MoveLeft(distance);
	}
	else if(input->IsKeyPressed(DIK_D)) {
		MoveRight(distance);
	}

	if(input->IsKeyPressed(DIK_SPACE)) {
		MoveUp(distance);
	}
	else if(input->IsKeyPressed(DIK_Z)) {
		MoveDown(distance);
	}

	// Handle mouse input
	int mouseX, mouseY;
	input->GetMouseLocation(mouseX, mouseY);
	input->ResetMouse();

	float YawToAdd = mouseX * .12f;
    float PitchToAdd = mouseY * .12f;

	m_rotation.x += PitchToAdd;
	m_rotation.y += YawToAdd;
}

void Camera::GetViewMatrix(D3DXMATRIX& viewMatrix)
{
	viewMatrix = m_viewMatrix;
	return;
}