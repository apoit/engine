#include "..\..\include\Scene\SceneManager.h"


SceneManager::SceneManager(void)
{
	m_RenderManager = NULL;
	m_Scene = NULL;
}


SceneManager::~SceneManager(void)
{
}

bool SceneManager::Initialize(RenderManager* renderer, HWND hwnd) {
	m_RenderManager = renderer;
	m_hwnd = hwnd;

	m_Scene = new Scene();
	if(!m_Scene) {
		return false;
	}

	return true;
}

void SceneManager::Shutdown() {
	if(m_Scene) {
		m_Scene->Shutdown();
		delete m_Scene;
		m_Scene = NULL;
	}
}

bool SceneManager::Draw() {
	return true;
}