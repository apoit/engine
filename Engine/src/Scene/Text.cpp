#include "..\..\include\Scene\Text.h"


Text::Text(void)
{
	m_Font = 0;
	m_Shader = 0;
	m_Sentence = 0;

	isUpdated = true;

	m_D3D = 0;
}


Text::~Text(void)
{
}

bool Text::InitializeSentence(int maxLength, D3DManager* D3D)
{
	VertexType* vertices;
	unsigned long* indices;
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData, indexData;
	HRESULT result;
	int i;


	// Create a new sentence object.
	m_Sentence = new SentenceType;
	if(!m_Sentence)
	{
		return false;
	}

	// Initialize the sentence buffers to null.
	(m_Sentence)->vertexBuffer = 0;
	(m_Sentence)->indexBuffer = 0;

	// Set the maximum length of the sentence.
	(m_Sentence)->maxLength = maxLength;

	// Set the number of vertices in the vertex array.
	(m_Sentence)->vertexCount = 6 * maxLength;

	// Set the number of indexes in the index array.
	(m_Sentence)->indexCount = (m_Sentence)->vertexCount;

	// Create the vertex array.
	vertices = new VertexType[(m_Sentence)->vertexCount];
	if(!vertices)
	{
		return false;
	}

	// Create the index array.
	indices = new unsigned long[(m_Sentence)->indexCount];
	if(!indices)
	{
		return false;
	}

	// Initialize vertex array to zeros at first.
	memset(vertices, 0, (sizeof(VertexType) * (m_Sentence)->vertexCount));

	// Initialize the index array.
	for(i=0; i<(m_Sentence)->indexCount; i++)
	{
		indices[i] = i;
	}

	// Set up the description of the dynamic vertex buffer.
	vertexBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	vertexBufferDesc.ByteWidth = sizeof(VertexType) * (m_Sentence)->vertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the vertex data.
	vertexData.pSysMem = vertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	// Create the vertex buffer.
	result = D3D->GetDevice()->CreateBuffer(&vertexBufferDesc, &vertexData, &(m_Sentence)->vertexBuffer);
	if(FAILED(result))
	{
		return false;
	}

	// Set up the description of the static index buffer.
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * (m_Sentence)->indexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	// Give the subresource structure a pointer to the index data.
	indexData.pSysMem = indices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	// Create the index buffer.
	result = D3D->GetDevice()->CreateBuffer(&indexBufferDesc, &indexData, &(m_Sentence)->indexBuffer);
	if(FAILED(result))
	{
		return false;
	}

	// Release the vertex array as it is no longer needed.
	delete [] vertices;
	vertices = 0;

	// Release the index array as it is no longer needed.
	delete [] indices;
	indices = 0;

	// Set our D3D manager
	m_D3D = D3D;

	// Create the camera object.
	Camera* baseCamera = new Camera;
	if(!baseCamera)
	{
		return false;
	}

	// Initialize a base view matrix with the camera for 2D user interface rendering.
	baseCamera->SetPosition(0.0f, 0.0f, -1.0f);
	baseCamera->Render();
	baseCamera->GetViewMatrix(m_baseViewMatrix);

	// Release the camera
	if(baseCamera)
	{
		delete baseCamera;
		baseCamera = 0;
	}

	return true;
}

void Text::UpdateSentence(std::string text, int positionX, int positionY, float red, float green, float blue) {
	// Set our variables
	m_Text = text;
	m_PositionX = positionX;
	m_PositionY = positionY;
	m_red = red;
	m_green = green;
	m_blue = blue;

	// Set updated to true
	isUpdated = false;
}

bool Text::isLoaded() {
	return m_Font->isLoaded() && m_Shader->isLoaded();
}

bool Text::UpdateBuffers() {
	int numLetters;
	VertexType* vertices;
	float drawX, drawY;
	HRESULT result;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	VertexType* verticesPtr;

	// Store the color of the sentence.
	m_Sentence->red = m_red;
	m_Sentence->green = m_green;
	m_Sentence->blue = m_blue;

	// Get the number of letters in the sentence.
	numLetters = (int)strlen(m_Text.c_str());

	// Check for possible buffer overflow.
	if(numLetters > m_Sentence->maxLength)
	{
		return false;
	}

	// Create the vertex array.
	vertices = new VertexType[m_Sentence->vertexCount];
	if(!vertices)
	{
		return false;
	}

	// Initialize vertex array to zeros at first.
	memset(vertices, 0, (sizeof(VertexType) * m_Sentence->vertexCount));

	// Calculate the X and Y pixel position on the screen to start drawing to.
	drawX = (float)(((m_D3D->GetScreenWidth() / 2) * -1) + m_PositionX);
	drawY = (float)((m_D3D->GetScreenHeight() / 2) - m_PositionY);

	// Use the font class to build the vertex array from the sentence text and sentence draw location.
	m_Font->BuildVertexArray((void*)vertices, const_cast<char*>(m_Text.c_str()), drawX, drawY);

	// Lock the vertex buffer so it can be written to.
	result = m_D3D->GetDeviceContext()->Map(m_Sentence->vertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	if(FAILED(result))
	{
		return false;
	}

	// Get a pointer to the data in the vertex buffer.
	verticesPtr = (VertexType*)mappedResource.pData;

	// Copy the data into the vertex buffer.
	memcpy(verticesPtr, (void*)vertices, (sizeof(VertexType) * m_Sentence->vertexCount));

	// Unlock the vertex buffer.
	m_D3D->GetDeviceContext()->Unmap(m_Sentence->vertexBuffer, 0);

	// Release the vertex array as it is no longer needed.
	delete [] vertices;
	vertices = 0;

	// Set our update to false
	isUpdated = true;

	return true;
}

bool Text::Render(D3DXMATRIX worldMatrix, D3DXMATRIX orthoMatrix)
{
	bool result;

	if(isLoaded()) {
		// If we need to update our buffers, do so
		result = UpdateBuffers();
		if(!result)
		{
			return false;
		}

		// Then render our sentence
		result = RenderSentence(worldMatrix, orthoMatrix);
		if(!result)
		{
			return false;
		}
	}

	return true;
}

bool Text::RenderSentence(D3DXMATRIX worldMatrix, D3DXMATRIX orthoMatrix)
{
	unsigned int stride, offset;
	D3DXVECTOR4 pixelColor;
	bool result;


	// Set vertex buffer stride and offset.
	stride = sizeof(VertexType); 
	offset = 0;

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	m_D3D->GetDeviceContext()->IASetVertexBuffers(0, 1, &m_Sentence->vertexBuffer, &stride, &offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	m_D3D->GetDeviceContext()->IASetIndexBuffer(m_Sentence->indexBuffer, DXGI_FORMAT_R32_UINT, 0);

	// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	m_D3D->GetDeviceContext()->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	// Create a pixel color vector with the input sentence color.
	pixelColor = D3DXVECTOR4(m_Sentence->red, m_Sentence->green, m_Sentence->blue, 1.0f);

	// Render the text using the font shader.
	result = m_Shader->Render(m_D3D->GetDeviceContext(), m_Sentence->indexCount, worldMatrix, m_baseViewMatrix, orthoMatrix, m_Font->GetTexture(), 
				      pixelColor);
	if(!result)
	{
		false;
	}

	return true;
}