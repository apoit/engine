#include "..\..\include\System\SystemManager.h"


SystemManager::SystemManager(int nShowCmd)
{
	m_nShowCmd = nShowCmd;
	m_RenderManager = 0;
	m_ThreadManager = 0;
	m_InputManager = 0;
	m_ResourceManager = 0;
	m_Fps = 0;
}

SystemManager::~SystemManager(void)
{
}

bool SystemManager::Initialize() {
	m_applicationName = L"Engine";
	int screenWidth, screenHeight;
	bool result;

	InitializeWindow(screenWidth, screenHeight);

	// Create the input manager
	m_InputManager = new InputManager();
	if(!m_InputManager) {
		return false;
	}

	// Initialize the input manager
	result = m_InputManager->Initialize(m_hInstance, m_hwnd, screenWidth, screenHeight);
	if(!result) {
		return false;
	}

	// Create the thread manager
	m_ThreadManager = new ThreadManager();
	if(!m_ThreadManager) {
		return false;
	}

	// Initialize the thread manager
	result = m_ThreadManager->Initialize(6);
	if(!result) {
		return false;
	}

	// Create the graphics object.  This object will handle rendering all the graphics for this application.
	m_RenderManager = new RenderManager();
	if(!m_RenderManager)
	{
		return false;
	}

	// Initialize the graphics object.
	result = m_RenderManager->Initialize(screenWidth, screenHeight, m_hwnd);
	if(!result)
	{
		return false;
	}

	// Create the resource manager
	m_ResourceManager = new ResourceManager();
	if(!m_ResourceManager)
	{
		return false;
	}

	// Initialize the resource manager
	result = m_ResourceManager->Initialize(m_RenderManager, m_ThreadManager, m_hwnd);
	if(!m_ResourceManager)
	{
		return false;
	}

	// Create the fps object.
	m_Fps = new Fps;
	if(!m_Fps)
	{
		return false;
	}

	// Initialize the fps object.
	m_Fps->Initialize();

	// Create the timer object.
	m_Timer = new Timer;
	if(!m_Timer)
	{
		return false;
	}

	// Initialize the timer object.
	result = m_Timer->Initialize();
	if(!result)
	{
		MessageBox(m_hwnd, L"Could not initialize the Timer object.", L"Error", MB_OK);
		return false;
	}

	return true;
}

void SystemManager::Run() {
	bool result;

	// this struct holds Windows event messages
    MSG msg = {0};

	// Create game logic instance
	GameLogic* m_GameLogic = new GameLogic(m_ResourceManager, m_RenderManager, m_Fps);

	// Start the render loop
	std::function<void()> f = [=] {
		// Render the scene
		bool result;
		while(TRUE && !m_ThreadManager->isStopped()) {
			result = m_RenderManager->Draw(m_GameLogic->getScene());

			// Update fps
			m_Fps->Frame();

			if(!result) break;
		}
	};

	m_ThreadManager->enqueue(f);

	// Try to initialize game logic, catch any exceptions
	try {
		m_GameLogic->Initialize();
	}
	catch(...) {
		return;
	}

	// Start the game logic update loop
	std::function<void()> g = [=] {
		while(TRUE && !m_ThreadManager->isStopped()) {
			m_GameLogic->Update(m_InputManager, m_Timer->GetTime());

			// Update timer
			m_Timer->Frame();
		}
	};

	m_ThreadManager->enqueue(g);

    // Enter the infinite message loop
    while(TRUE)
    {
        // Check to see if any messages are waiting in the queue
        if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            // translate keystroke messages into the right format
            TranslateMessage(&msg);

            // send the message to the WindowProc function
            DispatchMessage(&msg);

            // check to see if it's time to quit
            if(msg.message == WM_QUIT)
                break;
		}

		// Check if we have an exception in one of our threads
        if(m_ThreadManager->isStopped()) {
			break;
        }

		// Update the input for this frame in the main thread
		result = m_InputManager->Frame();
		if(!result)
		{
			MessageBox(m_hwnd, L"Input Processing Failed", L"Error", MB_OK);
			break;
		}

		// Check if the user wants to exit
		if(m_InputManager->IsKeyPressed(DIK_ESCAPE))
		{
			break;
		}
    }
}

// Shut down our subsystems in reverse order
void SystemManager::Shutdown() {
	// Shut down our threads
	m_ThreadManager->Shutdown();

	// Release the timer object.
	if(m_Timer)
	{
		delete m_Timer;
		m_Timer = 0;
	}

	// Release the fps object.
	if(m_Fps)
	{
		delete m_Fps;
		m_Fps = 0;
	}

	// Release the input manager
	if(m_InputManager)
	{
		m_InputManager->Shutdown();
		delete m_InputManager;
		m_InputManager = 0;
	}

	// Release the ResourceManager object.
	if(m_ResourceManager)
	{
		delete m_ResourceManager;
		m_ResourceManager = 0;
	}

	// Release the graphics object.
	if(m_RenderManager)
	{
		m_RenderManager->Shutdown();
		delete m_RenderManager;
		m_RenderManager = 0;
	}

	if(m_ThreadManager) {
		delete m_ThreadManager;
		m_ThreadManager = 0;
	}

	// Shutdown the window.
	ShutdownWindows();
	
	return;
}

void SystemManager::InitializeWindow(int& screenWidth, int& screenHeight) {
	// this struct holds information for the window class
    WNDCLASSEX wc;
	DEVMODE dmScreenSettings;
	int posX, posY;

    // clear out the window class for use
    ZeroMemory(&wc, sizeof(WNDCLASSEX));

	// Get the instance of this application.
	m_hInstance = GetModuleHandle(NULL);

    // fill in the struct with the needed information
    wc.cbSize = sizeof(WNDCLASSEX);
    wc.style = CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc = WindowProc;
    wc.hInstance = m_hInstance;
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH)COLOR_WINDOW;
    wc.lpszClassName = L"MainWindow";

    // register the window class
    RegisterClassEx(&wc);

	// Determine the resolution of the clients desktop screen.
	screenWidth  = GetSystemMetrics(SM_CXSCREEN);
	screenHeight = GetSystemMetrics(SM_CYSCREEN);

	// If windowed then set it to 800x600 resolution.
	screenWidth  = 1280;
	screenHeight = 800;

	RECT wr = {0, 0, screenWidth, screenHeight};    // set the size, but not the position
	AdjustWindowRect(&wr, WS_OVERLAPPEDWINDOW, FALSE);    // adjust the size

	// Place the window in the middle of the screen.
	posX = (GetSystemMetrics(SM_CXSCREEN) - screenWidth)  / 2;
	posY = (GetSystemMetrics(SM_CYSCREEN) - screenHeight) / 2;

    // create the window and use the result as the handle
    m_hwnd = CreateWindowEx(NULL,
                          L"MainWindow",    // name of the window class
						  m_applicationName,    // title of the window
                          WS_OVERLAPPEDWINDOW,    // window style
                          posX,    // x-position of the window
                          posY,    // y-position of the window
                          wr.right - wr.left,    // width of the window
						  wr.bottom - wr.top,    // height of the window
                          NULL,    // we have no parent window, NULL
                          NULL,    // we aren't using menus, NULL
                          m_hInstance,    // application handle
                          NULL);    // used with multiple windows, NULL

    // display the window on the screen
    ShowWindow(m_hwnd, m_nShowCmd);
}

void SystemManager::ShutdownWindows()
{
	// Show the mouse cursor.
	ShowCursor(true);

	// Fix the display settings if leaving full screen mode.
	if(FULL_SCREEN)
	{
		ChangeDisplaySettings(NULL, 0);
	}

	// Remove the window.
	DestroyWindow(m_hwnd);
	m_hwnd = NULL;

	// Remove the application instance.
	UnregisterClass(m_applicationName, m_hInstance);
	m_hInstance = NULL;

	return;
}

// this is the main message handler for the program
LRESULT CALLBACK WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    // sort through and find what code to run for the message given
    switch(message)
    {
        // this message is read when the window is closed
        case WM_DESTROY:
            {
                // close the application entirely
                PostQuitMessage(0);
                return 0;
            } break;
    }

    // Handle any messages the switch statement didn't
    return DefWindowProc (hWnd, message, wParam, lParam);
}