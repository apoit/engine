#include "..\..\include\Rendering\RenderManager.h"


RenderManager::RenderManager(void)
{
	m_D3D = NULL;
}


RenderManager::~RenderManager(void)
{
}

bool RenderManager::Initialize(int screenWidth, int screenHeight, HWND hwnd) {
	bool result;

	m_D3D = new D3DManager();
	if(!m_D3D) return false;

	// Initialize the Direct3D manager
	result = m_D3D->Initialize(screenWidth, screenHeight, VSYNC_ENABLED, hwnd, FULL_SCREEN, SCREEN_DEPTH, SCREEN_NEAR);
	if(!result)
	{
		MessageBox(hwnd, L"Could not initialize Direct3D", L"Error", MB_OK);
		return false;
	}

	return true;
}

void RenderManager::Shutdown() {
	if(m_D3D)
	{
		m_D3D->Shutdown();
		delete m_D3D;
		m_D3D = NULL;
	}
}

D3DManager* RenderManager::getD3D() {
	return m_D3D;
}

bool RenderManager::Draw(Scene* scene) {
	bool result;

	// Render the graphics scene.
	result = Render(scene);
	if(!result)
	{
		return false;
	}

	return true;
}

bool RenderManager::Render(Scene* scene) {
	D3DXMATRIX viewMatrix, projectionMatrix, worldMatrix, orthoMatrix;
	bool result;

	// Clear the buffers to begin the scene.
	m_D3D->BeginScene(0.0f, 0.0f, 0.0f, 1.0f);

	// If the scene is loaded
	if(scene && scene->m_Camera) {
		// Generate the view matrix based on the camera's position.
		scene->m_Camera->Render();

		// Get the world, view, and projection matrices from the camera and d3d objects.
		scene->m_Camera->GetViewMatrix(viewMatrix);
		m_D3D->GetWorldMatrix(worldMatrix);
		m_D3D->GetProjectionMatrix(projectionMatrix);
		m_D3D->GetOrthoMatrix(orthoMatrix);

		// Draw the models in the scene
		for(unsigned int i = 0; i < scene->m_Entities.size(); i++) {
			result = scene->m_Entities[i]->Render(worldMatrix, viewMatrix, projectionMatrix, m_D3D);
			if(!result) {
				return false;
			}
		}

		// Turn off the Z buffer to begin all 2D rendering.
		m_D3D->TurnZBufferOff();

		// Turn on the alpha blending before rendering the text.
		m_D3D->TurnOnAlphaBlending();

		// Render all the text in the scene
		for(unsigned int i = 0; i < scene->m_Texts.size(); i++) {
			result = scene->m_Texts[i]->Render(worldMatrix, orthoMatrix);
			if(!result) {
				return false;
			}
		}

		// Turn off alpha blending after rendering the text.
		m_D3D->TurnOffAlphaBlending();

		// Turn the Z buffer back on now that all 2D rendering has completed.
		m_D3D->TurnZBufferOn();
	}

	// Present the rendered scene to the screen.
	m_D3D->EndScene();

	return true;
}