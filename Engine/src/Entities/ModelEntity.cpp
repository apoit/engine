#include "..\..\include\Entities\ModelEntity.h"

ModelEntity::ModelEntity(void) :
	GenericEntity()
{
	m_Model = 0;
	m_Shader = 0;
	m_Texture = 0;
}

ModelEntity::~ModelEntity(void)
{

}

bool ModelEntity::isLoaded() {
	return m_Model->isLoaded() && m_Shader->isLoaded() && m_Texture->isLoaded();
}

bool ModelEntity::Render(D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix, D3DManager* D3D) {
	bool result;

	if(isLoaded()) {
		// Move the model to the location it should be rendered at.
		D3DXMatrixTranslation(&worldMatrix, m_Position.x, m_Position.y, m_Position.z); 

		// Put the model vertex and index buffers on the graphics pipeline to prepare them for drawing.
		m_Model->Render(D3D->GetDeviceContext());

		// Render the model using the shader.
		result = m_Shader->Render(D3D->GetDeviceContext(), m_Model->GetIndexCount(), worldMatrix, 
									viewMatrix, projectionMatrix, m_Texture->GetTexture());
		if(!result)
		{
			return false;
		}

		// Reset to the original world matrix.
		D3D->GetWorldMatrix(worldMatrix);
	}

	return true;
}