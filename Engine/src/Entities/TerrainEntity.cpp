#include "..\..\include\Entities\TerrainEntity.h"


TerrainEntity::TerrainEntity(void) : 
	GenericEntity()
{
	m_Terrain = 0;
	m_Shader = 0;
}


TerrainEntity::~TerrainEntity(void)
{
}

bool TerrainEntity::isLoaded() {
	return m_Terrain->isLoaded() && m_Shader->isLoaded();
}

bool TerrainEntity::Render(D3DXMATRIX worldMatrix, D3DXMATRIX viewMatrix, D3DXMATRIX projectionMatrix, D3DManager* D3D) {
	bool result;

	if(isLoaded()) {
		// Move the model to the location it should be rendered at.
		D3DXMatrixTranslation(&worldMatrix, m_Position.x, m_Position.y, m_Position.z); 

		// Put the model vertex and index buffers on the graphics pipeline to prepare them for drawing.
		m_Terrain->Render(D3D->GetDeviceContext());

		D3DXVECTOR4 ambient(0.05f, 0.05f, 0.05f, 1.0f);
		D3DXVECTOR4 diffuse(1.0f, 1.0f, 1.0f, 1.0f);
		D3DXVECTOR3 direction(0.2f, -0.2f, 0.75f);

		// Render the model using the shader.
		result = m_Shader->Render(D3D->GetDeviceContext(), m_Terrain->GetIndexCount(), worldMatrix, 
									viewMatrix, projectionMatrix, ambient, diffuse, direction);
		if(!result)
		{
			return false;
		}

		// Reset to the original world matrix.
		D3D->GetWorldMatrix(worldMatrix);
	}

	return true;
}