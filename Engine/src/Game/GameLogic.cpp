#include "..\..\include\Game\GameLogic.h"


GameLogic::GameLogic(ResourceManager* resourceManager, RenderManager* renderManager, Fps* fps)
{
	m_ResourceManager = resourceManager;
	m_RenderManager = renderManager;
	m_Fps = fps;

	m_Scene = 0;
}


GameLogic::~GameLogic(void)
{
}

void GameLogic::Initialize() {
	// Create the scene
	m_Scene = new Scene;

	// Load a shader
	TextureShader* m_Shader = m_ResourceManager->newResource<TextureShader>(L"../Engine/Resources/Shaders/ModelVertexShader.hlsl", L"../Engine/Resources/Shaders/ModelPixelShader.hlsl");

	// Create the camera object.
	m_Scene->m_Camera = new Camera;

	// Set the initial position of the camera.
	m_Scene->m_Camera->SetPosition(0.0f, 0.0f, -10.0f);

	// Create the text object
	Text* m_Text = new Text;
	m_Text->m_Shader = m_ResourceManager->newResource<FontShader>(L"../Engine/Resources/Shaders/FontVertexShader.hlsl", L"../Engine/Resources/Shaders/FontPixelShader.hlsl");
	m_Text->m_Font = m_ResourceManager->newResource<Font>(L"../Engine/Resources/data/fontdata.txt", L"../Engine/Resources/data/font.dds");
	m_Text->InitializeSentence(32, m_RenderManager->getD3D());
	m_Text->UpdateSentence("", 100, 100, 0.0f, 0.0f, 1.0f);

	// Put the text in the scene
	m_Scene->m_Texts.push_back(m_Text);

	// Create the terrain object
	TerrainEntity* m_Entity = new TerrainEntity;
	m_Entity->m_Shader = m_ResourceManager->newResource<ColorShader>(L"../Engine/Resources/Shaders/ColorVertexShader.hlsl", L"../Engine/Resources/Shaders/ColorPixelShader.hlsl");
	m_Entity->m_Terrain = m_ResourceManager->newResource<Terrain>(L"../Engine/Resources/data/heightmap01.bmp");
	m_Entity->m_Position = D3DXVECTOR3(0.0f, 0.0f, 0.0f);

	// Put the text in the scene
	m_Scene->m_Entities.push_back(m_Entity);
}

void GameLogic::Update(InputManager* input, float frameTime) {
	// Run game code
	m_Scene->m_Camera->Update(input, frameTime * 2);

	// Update our sentence
	std::ostringstream convert;
	convert << "FPS: " << m_Fps->GetFps();
	m_Scene->m_Texts[0]->UpdateSentence(convert.str(), 100, 100, 0.0f, 0.0f, 1.0f);
}

Scene* GameLogic::getScene() {
	return m_Scene;
}